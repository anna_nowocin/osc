import { httpService } from '@/services/HttpService';

class NewVirtualMachineService {
  getData() {
    return httpService.doGet('data.json');
  }
}

export const newVirtualMachineService = new NewVirtualMachineService();
