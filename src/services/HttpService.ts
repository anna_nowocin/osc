import axios from 'axios';

class HttpService {
  baseUrl = '/api/';

  doGet(url: string, params: any = {}) {
    return axios.get(`${this.baseUrl}${url}`, { params }).then(response => {
      return response.data;
    });
  }
}

export const httpService = new HttpService();
